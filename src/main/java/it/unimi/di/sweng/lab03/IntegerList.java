package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	private Element head;
	private Element tail;
	
	private class Element{
		private int value;
		private Element next;	
	}
	
	public IntegerList(){
		this.head = null;
		this.tail = null;
	}
	
	public IntegerList(String string) {
		if(string == ""){
			head = null;
			tail = null;
		} else {
			for(int i = 0; i < string.length(); i++)
				if(!Character.isDigit(string.charAt(i)) && string.charAt(i) != ' ')
					throw new IllegalArgumentException("Caratteri non consentiti");
			int f = 0, i = 0;
			while(i < string.length()) {
				f = i;
				while(f < string.length() && Character.isDigit(string.charAt(f)))
					f++;
				if(f > i){
					this.addLast(Integer.parseInt(string.substring(i, f)));			
					i = f + 1;
				}
			}
		}
	}

	public String toString(){
		StringBuilder result = new StringBuilder();
		result.append('[');
		Element curr = head;
		
		while(curr != null){
			inserisciSpazioSePrimo(result, curr);
			result.append(curr.value);
			curr = curr.next;
		}
		result.append(']');
		return result.toString();
	}

	private void inserisciSpazioSePrimo(StringBuilder result, Element e) {
		if(e != head)
			result.append(' ');
	}

	public void addLast(int i) {
		Element toAdd = new Element();
		toAdd.value = i;
		toAdd.next = null;
		if(tail != null)
			tail.next = toAdd;
		tail = toAdd;
		if(head == null)
			head = toAdd;
	}

	public void addFirst(int i) {
		Element toAdd = new Element();
		toAdd.value = i;
		if(head != null)
			toAdd.next = head;
		head = toAdd;
		if(tail == null)
			tail = toAdd;
	}

	public boolean removeFirst() {
		if(head != null){
			head = head.next;
			return true;
		} else
			return false;		
	}

	public boolean removeLast() {
		if(tail != null){
			Element curr = head;
			Element prec = new Element();
			int cont = 0;
			while(curr.next != null){
				prec = curr;
				curr = curr.next;
				cont ++;
			}
			if(cont >= 1){
				prec.next = null;
				tail = prec;
			} else {
				head = null;
				tail = null;
			}
			return true;
		} else 
			return false;
	}

	public boolean remove(int i) {
		Element curr = head;
		Element prec = new Element();
		while(curr.next != null){
			if(curr.value == i){
				prec.next = curr.next;
				return true;				
			}
			prec = curr;
			curr = curr.next;
		}
		return false;
	}

	public boolean removeAll(int i) {
		Element curr = head;
		Element prec = new Element();
		boolean flag = false;
		while(curr.next != null){
			if(curr.value == i){
				prec.next = curr.next;
				flag = true;				
			}
			prec = curr;
			curr = curr.next;
		}
		return flag;
	}	
}