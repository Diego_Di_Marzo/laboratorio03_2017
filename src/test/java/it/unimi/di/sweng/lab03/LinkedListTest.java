package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void testToStringCostruttoreVuoto() {
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void testAddLast() {
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void testAddWithString() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreRobusto() {
		list = new IntegerList("1 2 aaa");
	}
	
	@Test
	public void testAddFirst() {
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void testRemoveFirst() {
		list = new IntegerList("1 2");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[2]");
		list = new IntegerList("2");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("");
		assertThat(list.removeFirst()).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[]");		
	}

	@Test
	public void testRemoveLast() {
		list = new IntegerList("7 10");
		assertThat(list.removeLast()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[7]");
		list = new IntegerList("7");
		assertThat(list.removeLast()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("");
		assertThat(list.removeLast()).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void testRemove() {
		list = new IntegerList("1 2 3 4 3 5");
		assertThat(list.remove(2)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 3 4 3 5]");
		assertThat(list.remove(3)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");
		assertThat(list.remove(6)).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");		
	}
	
	@Test
	public void testRemoveAll() {
		list = new IntegerList("1 2 3 4 3 5");
		assertThat(list.removeAll(3)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 2 4 5]");
		assertThat(list.removeAll(6)).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[1 2 4 5]");	
	}
}